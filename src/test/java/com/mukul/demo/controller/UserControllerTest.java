package com.mukul.demo.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.mukul.demo.model.User;
import com.mukul.demo.service.UserService;
import org.junit.Test;
import org.junit.Before;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import java.util.Arrays;

import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.is;
import static org.mockito.BDDMockito.given;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;

@RunWith(SpringRunner.class)
@WebMvcTest
public class UserControllerTest {
    @Autowired
    private MockMvc mvc;

    @MockBean
    private UserService userService;

    private ObjectMapper objectMapper = new ObjectMapper();

    private User user1;
    private User user2;

    private final String user1_id = "uid01";
    private final String user2_id = "uid02";
    private final String user1_name = "name01";
    private final String user2_name = "name02";
    private final String user1_place = "place01";
    private final String user2_place = "place02";

    @Before
    public void setup() {
        user1 = new User();
        user1.setId(user1_id);
        user1.setName(user1_name);
        user1.setPlace(user1_place);

        user2 = new User();
        user2.setId(user2_id);
        user2.setName(user2_name);
        user2.setPlace(user2_place);
    }

    @Test
    public void getDemo_thenReturnStatusOk() throws Exception {

        mvc.perform(get("/api/v1/demo/")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());
    }

    @Test
    public void createUser_thenReturnStatusCreated() throws Exception {
        given(userService.createUser(user1)).willReturn(user1);

        String jsonString = objectMapper.writeValueAsString(user1);

        mvc.perform(post("/api/v1/demo/users/")
                .contentType(MediaType.APPLICATION_JSON).content(jsonString))
                .andExpect(status().isCreated());
    }

    @Test
    public void getAllUsers_thenReturnJsonArray() throws Exception {
        given(userService.getAllUsers()).willReturn(Arrays.asList(user1));

        mvc.perform(get("/api/v1/demo/users/")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$", hasSize(1)))
                .andExpect(jsonPath("$[0].name", is(user1.getName())));
    }

    @Test
    public void getUserById_thenReturnJson() throws Exception {
        given(userService.getUserById(user1_id)).willReturn(user1);

        mvc.perform(get("/api/v1/demo/users/{id}", user1_id)
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.name", is(user1.getName())));
    }

    @Test
    public void getUsersByName_thenReturnJsonArray() throws Exception {
        given(userService.getUsersByName(user1_name)).willReturn(Arrays.asList(user1));

        mvc.perform(get("/api/v1/demo/users/name?name="+user1_name)
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$", hasSize(1)))
                .andExpect(jsonPath("$[0].name", is(user1.getName())));
    }

    @Test
    public void getUsersByPlace_thenReturnJsonArray() throws Exception {
        given(userService.getUsersByPlace(user1_place)).willReturn(Arrays.asList(user1));

        mvc.perform(get("/api/v1/demo/users/place?place="+user1_place)
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$", hasSize(1)))
                .andExpect(jsonPath("$[0].place", is(user1.getPlace())));
    }

    @Test
    public void updateUser_thenReturnStatusOk() throws Exception {
        given(userService.updateUser(user1_id, user1)).willReturn(user1);

        String jsonString = objectMapper.writeValueAsString(user1);

        mvc.perform(put("/api/v1/demo/users/{id}", user1_id)
                .contentType(MediaType.APPLICATION_JSON).content(jsonString))
                .andExpect(status().isOk());
    }

    @Test
    public void deleteUser_thenReturnStatusNoContent() throws Exception {
        given(userService.getUserById(user1_id)).willReturn(user1);

        Mockito.doNothing().when(userService).deleteUser(user1_id);

        mvc.perform(delete("/api/v1/demo/users/{id}", user1_id)
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isNoContent());
    }
}
